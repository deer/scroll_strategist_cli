use crate::{adapter::ItemStateAdapter, opts::Mode};
use scroll_strategist::{scroll, Stats};
use serde::Deserialize;
use smallvec::SmallVec;
use std::{fs::File, io, path::Path};

#[derive(Deserialize, Debug)]
pub struct Input {
    pub mode: Option<Mode>,
    pub stats: SmallVec<[u16; 4]>,
    pub slots: u8,
    #[serde(default)]
    pub upfront_cost: u32,
    pub scrolls: Vec<Scroll>,
    pub goal: SmallVec<[u16; 4]>,
}

#[derive(Deserialize, Debug)]
pub struct Scroll {
    pub percent: f64,
    pub dark: bool,
    pub cost: u32,
    pub stats: SmallVec<[u16; 4]>,
}

pub fn read_from_file<P: AsRef<Path>>(
    json_path: P,
) -> io::Result<(ItemStateAdapter, Vec<scroll::Scroll>, Stats, Option<Mode>)> {
    let mut json_file = File::open(json_path)?;
    let json_input: Input = serde_json::from_reader(&mut json_file)?;

    Ok((
        ItemStateAdapter {
            slots: json_input.slots,
            stats: json_input.stats.into(),
            sunk_cost: json_input.upfront_cost,
        },
        json_input
            .scrolls
            .into_iter()
            .map(|s| {
                scroll::Scroll::new(
                    s.percent / 100.0,
                    s.dark,
                    s.cost,
                    s.stats.into(),
                )
            })
            .collect(),
        json_input.goal.into(),
        json_input.mode,
    ))
}
