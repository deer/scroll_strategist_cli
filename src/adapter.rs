use scroll_strategist::Stats;

pub struct ItemStateAdapter {
    pub slots: u8,
    pub stats: Stats,
    pub sunk_cost: u32,
}

pub trait MaybeCost {
    fn maybe_cost(&self) -> Option<u32>;
}

impl MaybeCost for () {
    fn maybe_cost(&self) -> Option<u32> {
        None
    }
}
impl MaybeCost for u32 {
    fn maybe_cost(&self) -> Option<u32> {
        Some(*self)
    }
}
