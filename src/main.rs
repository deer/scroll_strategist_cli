#![forbid(unsafe_code)]
#![deny(clippy::all)]
#![deny(deprecated)]

mod adapter;
mod display;
mod json;
mod opts;

use crate::display::{expectation_to_string, u32_to_string};
use adapter::{ItemStateAdapter, MaybeCost};
use clap::Parser;
use opts::Mode;
use scroll_strategist::{
    dfs::solve, dfs_c::solve_c, graph::ItemState, scroll::Scroll, stats, Stats,
};
use smallvec::SmallVec;
use std::io::{self, Write};

fn main() -> io::Result<()> {
    let cl_opts = opts::Opts::parse();

    let stdin = io::stdin();
    let mut input_buf = String::new();

    let (
        ItemStateAdapter {
            slots,
            stats,
            sunk_cost,
        },
        mut scrolls,
        goal,
        mode,
    ) = if let Some(json_path) = cl_opts.json {
        json::read_from_file(json_path)?
    } else {
        interactive_input(&stdin, &mut input_buf, cl_opts.mode)?
    };

    if cl_opts.null_scroll {
        scrolls.push(Scroll::null(stats.len()));
    }
    let mode = cl_opts.mode.or(mode).unwrap_or_default();

    match mode {
        Mode::SucProb => {
            let mut item_state = ItemState::<()>::new_exists(slots, stats);
            solve(&mut item_state, &scrolls, &goal);

            present_results(
                item_state,
                &stdin,
                &mut input_buf,
                cl_opts.noninteractive,
            )
        }
        Mode::SucExpCost => {
            let mut item_state =
                ItemState::<u32>::new_exists(slots, stats, sunk_cost);
            solve_c(&mut item_state, &scrolls, &goal);

            present_results(
                item_state,
                &stdin,
                &mut input_buf,
                cl_opts.noninteractive,
            )
        }
    }
}

fn present_results<C: MaybeCost>(
    item_state: ItemState<C>,
    stdin: &io::Stdin,
    input_buf: &mut String,
    noninteractive: bool,
) -> io::Result<()> {
    if noninteractive {
        if let ItemState::Exists {
            slots: _,
            stats: _,
            sunk_cost,
            child: Some(child),
        } = item_state
        {
            let (init_cost, total_str) =
                if let Some(c) = sunk_cost.maybe_cost() {
                    (f64::from(c), "total ")
                } else {
                    (0.0, "")
                };

            println!("Probability of success: {:.3}%", child.p_goal * 100.0);
            println!(
                "Expected {total_str}cost: {}",
                expectation_to_string(init_cost + child.exp_cost),
            );
            println!(
                "Expected {total_str}cost per success: {}",
                expectation_to_string(
                    (init_cost + child.exp_cost) / child.p_goal
                ),
            );
            println!(
                "Next scroll to use: {:.0}%",
                child.scroll().p_suc * 100.0,
            );
        }

        Ok(())
    } else {
        print!("=== Results ===");
        interactive_scrolling(&item_state, stdin, input_buf, -1.0)
    }
}

fn interactive_input(
    stdin: &io::Stdin,
    input_buf: &mut String,
    mode: Option<Mode>,
) -> io::Result<(ItemStateAdapter, Vec<Scroll>, Stats, Option<Mode>)> {
    println!(
        "=== Welcome to scroll_strategist_cli! ===
Just answer some questions about what you’re scrolling, what you’re scrolling
it with, & what you want your scrolling strategy to optimise for, and
scroll_strategist can work its magic~\n",
    );

    let mode = {
        let mut mode: Option<Mode> = mode;
        while mode.is_none() {
            input_buf.clear();

            print!("Optimisation mode [suc-prob (default)|suc-exp-cost]: ");
            io::stdout().flush()?;
            stdin.read_line(input_buf)?;

            input_buf.make_ascii_lowercase();
            mode = match input_buf.trim() {
                "" | "suc-prob" | "suc_prob" => Some(Mode::SucProb),
                "suc-exp-cost" | "suc_exp_cost" => Some(Mode::SucExpCost),
                _ => None,
            };
        }

        mode
    };

    let stats = {
        let mut stats: Option<Stats> = None;
        'stats: while stats.is_none() {
            input_buf.clear();

            print!("Stats: ");
            io::stdout().flush()?;
            stdin.read_line(input_buf)?;

            let mut stats_vec = SmallVec::new();
            for maybe_stat in input_buf
                .split(',')
                .filter(|s| !s.is_empty())
                .map(|s| s.trim().parse().ok())
            {
                if let Some(stat) = maybe_stat {
                    stats_vec.push(stat);
                } else {
                    continue 'stats;
                }
            }
            stats = Some(stats_vec.into());
        }

        stats.unwrap_or_else(|| unreachable!())
    };

    let slots = {
        let mut slots: Option<u8> = None;
        while slots.is_none() {
            input_buf.clear();

            print!("Slots remaining: ");
            io::stdout().flush()?;
            stdin.read_line(input_buf)?;

            slots = input_buf.trim().parse().ok();
        }

        slots.unwrap_or_else(|| unreachable!())
    };

    let mut scrolls = Vec::new();
    'scrolls: loop {
        let mut scroll = Scroll::new(0.0, false, 0, stats![]);

        loop {
            input_buf.clear();

            print!("Scroll {} %: ", scrolls.len() + 1);
            io::stdout().flush()?;
            stdin.read_line(input_buf)?;

            let trimmed = input_buf.trim();
            if trimmed.is_empty() && !scrolls.is_empty() {
                break 'scrolls;
            }
            if let Ok(percentage_points) = trimmed.parse::<f64>() {
                scroll.p_suc = percentage_points / 100.0;

                break;
            }
        }

        loop {
            input_buf.clear();

            print!("Scroll {} dark?: ", scrolls.len() + 1);
            io::stdout().flush()?;
            stdin.read_line(input_buf)?;

            input_buf.make_ascii_lowercase();
            match input_buf.trim() {
                "yes" | "y" | "true" => {
                    scroll.dark = true;

                    break;
                }
                "no" | "n" | "false" => {
                    scroll.dark = false;

                    break;
                }
                _ => (),
            }
        }

        loop {
            input_buf.clear();

            print!("Scroll {} cost: ", scrolls.len() + 1);
            io::stdout().flush()?;
            stdin.read_line(input_buf)?;

            if let Ok(cost) = input_buf.trim().parse() {
                scroll.cost = cost;

                break;
            }
        }

        'scroll_stats: loop {
            input_buf.clear();

            print!("Scroll {} stats: ", scrolls.len() + 1);
            io::stdout().flush()?;
            stdin.read_line(input_buf)?;

            let mut stats_vec = SmallVec::new();
            for maybe_stat in input_buf
                .split(',')
                .filter(|s| !s.is_empty())
                .map(|s| s.trim().parse().ok())
            {
                if let Some(stat) = maybe_stat {
                    stats_vec.push(stat);
                } else {
                    continue 'scroll_stats;
                }
            }

            if stats_vec.len() == stats.len() {
                scroll.stats = stats_vec.into();

                break;
            }
        }

        scrolls.push(scroll);
    }

    let goal = {
        let mut goal: Option<Stats> = None;
        'goal_stats: while goal.is_none() {
            input_buf.clear();

            print!("Goal: ");
            io::stdout().flush()?;
            stdin.read_line(input_buf)?;

            let mut stats_vec = SmallVec::new();
            for maybe_stat in input_buf
                .split(',')
                .filter(|s| !s.is_empty())
                .map(|s| s.trim().parse().ok())
            {
                if let Some(stat) = maybe_stat {
                    stats_vec.push(stat);
                } else {
                    continue 'goal_stats;
                }
            }

            if stats_vec.len() == stats.len() {
                goal = Some(stats_vec.into());
            }
        }

        goal.unwrap_or_else(|| unreachable!())
    };

    let mut sunk_cost = 0;
    if mode == Some(Mode::SucExpCost) {
        loop {
            input_buf.clear();

            print!("Upfront cost: ");
            io::stdout().flush()?;
            stdin.read_line(input_buf)?;

            if let Ok(c) = input_buf.trim().parse() {
                sunk_cost = c;

                break;
            }
        }
    }

    Ok((
        ItemStateAdapter {
            slots,
            stats,
            sunk_cost,
        },
        scrolls,
        goal,
        mode,
    ))
}

fn interactive_scrolling<C: MaybeCost>(
    item_state: &ItemState<C>,
    stdin: &io::Stdin,
    input_buf: &mut String,
    mut init_exp_cost: f64,
) -> io::Result<()> {
    println!();

    if let ItemState::Exists {
        slots: _,
        stats,
        sunk_cost,
        child,
    } = item_state
    {
        if let Some(child) = child {
            println!("Probability of success: {:.3}%", child.p_goal * 100.0);
            sunk_cost
                .maybe_cost()
                .inspect(|sc| println!("Sunk cost: {}", u32_to_string(*sc)));
            println!(
                "Expected further cost: {}",
                expectation_to_string(child.exp_cost),
            );
            let efcps = child.exp_cost / child.p_goal;
            println!(
                "Expected further cost per success: {}{}",
                expectation_to_string(efcps),
                if init_exp_cost >= 0.0 && efcps > init_exp_cost {
                    "(!)"
                } else {
                    ""
                },
            );
            sunk_cost.maybe_cost().inspect(|sc| {
                let etcps = (f64::from(*sc) + child.exp_cost) / child.p_goal;
                if init_exp_cost < 0.0 {
                    init_exp_cost = etcps;
                }
                println!(
                    "Expected total cost per success: {}",
                    expectation_to_string(etcps),
                )
            });
            println!(
                "Next scroll to use: {:.0}%",
                child.scroll().p_suc * 100.0,
            );

            loop {
                input_buf.clear();

                print!("Did the scroll pass? [y|n|b]: ");
                io::stdout().flush()?;
                stdin.read_line(input_buf)?;

                let outcomes = child.outcomes();
                input_buf.make_ascii_lowercase();
                match input_buf.trim() {
                    "yes" | "y" | "true" => {
                        if let Some(pass_outcome) = outcomes.pass() {
                            return interactive_scrolling(
                                pass_outcome,
                                stdin,
                                input_buf,
                                init_exp_cost,
                            );
                        }

                        println!("\nFinal stats: {stats}");

                        break;
                    }
                    "no" | "n" | "false" => {
                        if let Some(miss_outcome) = outcomes.miss() {
                            return interactive_scrolling(
                                miss_outcome,
                                stdin,
                                input_buf,
                                init_exp_cost,
                            );
                        }

                        println!("\nFinal stats: {stats}");

                        break;
                    }
                    "boom" | "b" | "boomed" => {
                        println!("\nR.I.P. :(");

                        return Ok(());
                    }
                    _ => (),
                }
            }
        } else {
            println!("Final stats: {stats}");
        }
    } else {
        println!("R.I.P. :(");
    }

    Ok(())
}
