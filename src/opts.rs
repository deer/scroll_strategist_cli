use clap::{Parser, ValueEnum};
use serde::Deserialize;

#[derive(Parser)]
#[command(version, author, about)]
pub struct Opts {
    /// The path to a JSON file to use for equipment & scroll information.
    #[arg(short, long)]
    pub json: Option<String>,
    /// Non-interactive mode will not interactively query for equipment &
    /// scroll information, and will not drop you into a quasi-shell after
    /// generating a scrolling strategy. Instead, it will exit immediately
    /// after emitting the first scroll choice.
    ///
    /// This option is invalid without `--json` because otherwise, there would
    /// be no source of equipment & scroll information.
    #[arg(
        help = "Non-interactive mode exits after initial output.",
        short,
        long,
        requires = "json"
    )]
    pub noninteractive: bool,
    /// `suc-prob` (the default mode) optimises solely to maximise the
    /// probability of success on a single “attempt” (meaning a single
    /// equipment item). It ignores the cost of each scroll when making
    /// decisions, except when needed to break an exact tie.
    ///
    /// `suc-exp-cost` attempts to minimise the per-success expected cost,
    /// assuming an arbitrary number of possible attempts.
    ///
    /// This option overrides any optimisation mode selected interactively or
    /// via JSON.
    #[arg(help = "What optimisation mode to use.", short, long, value_enum)]
    pub mode: Option<Mode>,
    /// Implicitly includes a “null scroll” as part of the scroll set.
    ///
    /// A null scroll has a 100% probability of success, is not dark, costs
    /// nothing, & grants no stats upon success.
    #[arg(
        help = "Include a “null scroll” as part of the scroll set.",
        short = 'z',
        long
    )]
    pub null_scroll: bool,
}

#[derive(Clone, Copy, Debug, PartialEq, Eq, ValueEnum, Deserialize)]
pub enum Mode {
    SucProb,
    SucExpCost,
}

impl Default for Mode {
    fn default() -> Self {
        Self::SucProb
    }
}
