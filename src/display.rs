//! Grossly inefficiently putting underscores into numerals. Wow.

/// Exactly one digit after the decimal point.
pub fn expectation_to_string(e: f64) -> String {
    let mut s = format!("{e:.1}");
    for i in (1..s.len().saturating_sub(4)).rev().step_by(3) {
        s.insert(i, '_');
    }

    s
}

pub fn u32_to_string(n: u32) -> String {
    let mut s = n.to_string();
    for i in (1..s.len().saturating_sub(2)).rev().step_by(3) {
        s.insert(i, '_');
    }

    s
}
