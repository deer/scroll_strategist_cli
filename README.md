# scroll\_strategist\_cli

A minimal &amp; simplistic
[<abbr title="command-line interface">CLI</abbr>](https://en.wikipedia.org/wiki/Command-line_interface) to the algorithms
provided by [scroll\_strategist](https://codeberg.org/deer/scroll_strategist).

## Building

Building scroll\_strategist\_cli requires the [Rust](https://www.rust-lang.org/) toolchain, which can be most easily obtained via [rustup](https://rustup.rs/).

The [Cargo\.toml](Cargo.toml) that comes with scroll\_strategist\_cli assumes that the scroll\_strategist library is its sibling in the filesystem tree…

```toml
[dependencies]
scroll_strategist = { path = "../scroll_strategist" }
```

…So it’s easiest to just clone them both into the same directory:

```bash
$ git clone https://codeberg.org/deer/scroll_strategist.git
$ git clone https://codeberg.org/deer/scroll_strategist_cli.git
$ ls -F
…  scroll_strategist/  scroll_strategist_cli/  …
```

The easiest way to just run scroll\_strategist\_cli once is to use `cargo run`:

```bash
cd scroll_strategist_cli
cargo run --release -- --help
```

For repeated use, scroll\_strategist\_cli can be built with either of the following commands:

```bash
cargo build --release
# OR
cargo rustc --release -- -C target-cpu=native
```

The latter command compiles specifically for your CPU, &amp; thus will make the resulting scroll\_strategist\_cli binary less portable, but will also make it run faster! &#x2728;

The resulting binary is `target/release/scroll_strategist_cli`. To make it easier to invoke, you can set up an alias like e\.g.:

```bash
alias ssc='…/scroll_strategist_cli/target/release/scroll_strategist_cli'
```

## Usage

For an explanation of the command-line options that scroll\_strategist\_cli accepts, run `scroll_strategist_cli --help`.

scroll\_strategist\_cli needs equipment item &amp; scroll information, and you might also provide an upfront cost and/or an optimisation mode. Upfront cost defaults to zero, &amp; the optimisation mode defaults to SucProb (see the “Optimisation modes” section of scroll\_strategist’s README). This information can be supplied interactively if neither `--noninteractive` nor `--json` are given on the command line. However, it’s usually easier to use `--json`/`-j`, so that edits can be made with your text editor of choice.

Here’s an example JSON file, with everything included (`"mode"` and `"upfront_cost"` are optional):

```json
{
  "mode": "SucExpCost",
  "stats": [17],
  "slots": 10,
  "upfront_cost": 1000000,
  "scrolls": [
    { "percent":  30, "dark": true,  "cost": 2000000, "stats": [3] },
    { "percent":  60, "dark": false, "cost": 7000000, "stats": [2] },
    { "percent":  70, "dark": true,  "cost": 3500000, "stats": [2] },
    { "percent": 100, "dark": false, "cost": 3200000, "stats": [1] }
  ],
  "goal": [35]
}
```

You can use already-existing JSON files as templates, to reduce some of the boilerplate here.

## Tips

---

**&#x2139;&#xfe0f;** Please also see the README for scroll\_strategist, especially the “Using the optimiser” &amp; “Example” sections!

---

- Many, if not most, realistic scrolling situations have characteristics lying somewhere in between the assumptions implicitly made by SucProb &amp; SucExpCost. It’s often a good idea to at least _consult_ both optimisation modes, unless you’re absolutely certain that one mode is preferred over the other.

### SucExpCost

- Take note of the initial value given for “Expected <mark>total</mark> cost per success”. If the “Expected <mark>further</mark> cost per success” ever exceeds that value, then you should probably give up &amp; start over, if possible!

  scroll\_strategist\_cli will warn you about this by putting “(!)” next to the “Expected further cost per success” value.

  Of course, this assumes that you really do have some arbitrary number of attempts that all start from the same initial state. If you do give up on an equip &amp; start over, in many cases it may be wise to hold onto the equip so that you can come back to it with a new strategy, if necessary.

## Legal

[<abbr title="GNU Affero General Public License">AGPL</abbr> <abbr title="version">v</abbr>3](https://www.gnu.org/licenses/agpl-3.0)+
